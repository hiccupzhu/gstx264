#!/usr/bin/python

import os
import subprocess
import re

output="tcpserversink port=8000 "
#output="udpsink host=192.168.8.204 port=20000 "

#tags="clockoverlay halign=left valign=bottom time-format=%Y/%m/%d-%H:%M:%S ! timeoverlay halign=right valign=bottom "
#tags="clockoverlay halign=left valign=bottom time-format=%Y/%m/%d-%H:%M:%S ! timeoverlay halign=center valign=center ypad=200 "
#tags="queue"



xopts="""
 byte-stream=true
 level=51
 profile=high
 bitrate=1500
 max-bitrate=1500
 rc-lookahead=40
 mb-tree=true
 stats-file=/tmp/x264.log
 
 option-string="
:ref=3
:me=hex:subme=7
:merange=16:nf=1:deblock=1,-3:weightp=1:scenecut=0
:keyint=300:min-keyint=25
:bframes=3
:threads=4:lookahead-threads=4:sliced-threads=false:b-pyramid=2:direct=spatial
"
""" 

xopts=xopts.replace("\n","");
xopts, num=re.subn(" +", " ", xopts);
#print "xopts=", xopts


cmd =  "gst-launch -v --gst-debug=h264dec:3 \
        udpsrc uri=udp://127.0.0.1:20000 ! queue ! mpegtsdemux name=demuxer \
        demuxer. ! queue ! ffdec_h264 ! \
        queue ! deinterlace method=4 fields=1 ! queue ! videoscale ! \
        video/x-raw-yuv,width=1280,height=720 ! queue ! mscenecut keyint=250 ! queue ! "
cmd += "x264enc " + xopts + " ! queue ! mpegtsmux name=muxer ! queue ! "
cmd += "tcpserversink port=8000 "
cmd += "demuxer. ! queue ! faad ! queue ! audioconvert ! queue ! audioresample ! \
       \"audio/x-raw-int,width=16,depth=16,rate=48000,channels=2,endianness=1234\" ! \
        queue ! faac outputformat=1 ! queue ! muxer."

cmd, num = re.subn(" +", " ", cmd);
print cmd;

os.system(cmd);



